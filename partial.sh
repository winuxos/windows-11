#!/bin/bash
# HYPER_VERSION="$(curl --silent https://api.github.com/repos/vercel/hyper/releases/latest | grep -Po '\"tag_name\": \"\K.*?(?=\")')"
# echo $HYPER_VERSION
# curl "https://github.com/vercel/hyper/releases/download/${HYPER_VERSION}/hyper_${HYPER_VERSION}_amd64.deb" -o "hyper_${HYPER_VERSION}_amd64.deb"
# sudo apt-get install -y -s "./hyper_${HYPER_VERSION}_amd64.deb"
curl https://releases.hyper.is/download/deb -o hyper.deb
sudo apt-get install -y -s ./hyper.deb
