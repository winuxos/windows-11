#! /bin/bash
alias dir=ls
alias cmd=wineconsole
alias cmd.exe=wineconsole
alias move=mv
alias cls=clear
alias copy=cp
alias chdir=cd
export WINEPREFIX=/
