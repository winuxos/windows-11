#! /bin/bash
# Update
sudo dpkg --add-architecture i386
sudo add-apt-repository multiverse
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
sudo rm microsoft.gpg
sudo add-apt-repository -y 'deb http://mirrors.kernel.org/ubuntu groovy main'
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo add-apt-repository -y 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
sudo apt update && sudo apt upgrade -y
# Install dependencies
# sudo apt-get install -y ./debs/libx11-xcb1_1.6.12-1_amd64.deb
# sudo apt-get install -y ./debs/libicu67_67.1-4_amd64.deb
# sudo apt-get install -y ./debs/libqt5core5a_5.14.2+dfsg-6_amd64.deb
# sudo apt-get install -y ./debs/libqt5dbus5_5.14.2+dfsg-6_amd64.deb
# sudo apt-get install -y ./debs/libqt5network5_5.14.2+dfsg-6_amd64.deb
# sudo apt-get install -y ./debs/libqt5gui5_5.14.2+dfsg-6_amd64.deb
# sudo apt-get install -y ./debs/libqt5widgets5_5.14.2+dfsg-6_amd64.deb
# sudo apt-get install -y ./debs/libqt5svg5_5.14.2-2_amd64.deb
# sudo apt-get install -y ./debs/qt5-gtk-platformtheme_5.14.2+dfsg-6_amd64.deb
sudo apt install -y --install-recommends xdm mutter x11-xserver-utils libchewing3 ttf-mscorefonts-installer xorg plymouth plymouth-theme-script plank microsoft-edge-dev libqt5gui5 winehq-devel wine-binfmt dosbox exe-thumbnailer fonts-wine q4wine network-manager network-manager-openvpn
# Possible additions: msitools
sudo apt install autoremove -y
sudo snap install skype
sudo snap install uno-calculator
# Copy scripts
sudo mkdir /etc/xdg/openbox/ -p
sudo cp autostart /etc/xdg/openbox/
cp .xinitrc ~/
# Windows backgrounds
sudo mkdir /usr/share/backgrounds -p
sudo cp ./bgs/* /usr/share/backgrounds
# Windows icons
sudo mkdir /usr/share/svgs -p
# sudo cp ./svgs/* /usr/share/svgs
git clone https://github.com/Templarian/MaterialDesign.git
find ./MaterialDesign/svg/ -name "*.svg" -maxdepth 1 -exec sudo mv -t /usr/share/svgs {} +
# Directories
mkdir ~/Desktop
mkdir ~/Documents
mkdir ~/Downloads
mkdir ~/Pictures
mkdir ~/Videos
mkdir ~/Music
# Windows Settings file
cp ./settings.json /home/*/.config/windtu/settings.json
sudo cp ./settings.json /etc/settings.json
# Wine
sudo mkdir -p /opt/wine/mono/wine-mono-5.1.1
sudo mkdir -p /usr/share/wine/gecko
curl https://dl.winehq.org/wine/wine-mono/5.1.1/wine-mono-5.1.1-x86.tar.xz -o wine-mono-5.1.1-x86.tar.xz
tar -xf wine-mono-5.1.1-x86.tar.xz -C /opt/wine/mono/wine-mono-5.1.1
curl http://dl.winehq.org/wine/wine-gecko/2.47.1/wine-gecko-2.47.1-x86.tar.bz2 -o wine-gecko-2.47.1-x86.tar.bz2
curl http://dl.winehq.org/wine/wine-gecko/2.47.1/wine-gecko-2.47.1-x86_64.tar.bz2 -o wine-gecko-2.47.1-x86_64.tar.bz2
tar -xf wine-gecko-2.47.1-x86.tar.bz2 -C /usr/share/wine/gecko
tar -xf wine-gecko-2.47.1-x86_64.tar.bz2 -C /usr/share/wine/gecko
export WINEPREFIX=""
mkdir /mnt/c/
mkdir /mnt/d/
ln -s / /mnt/z/
ln -s /home /mnt/c/Users/
# Hyper terminal
curl https://releases.hyper.is/download/deb -o hyper.deb
sudo apt-get install -y -s ./hyper.deb
# Misc
sudo fc-cache -f -v
# Defaults/Aliases
sudo update-alternatives --set x-www-browser /usr/bin/microsoft-edge-dev
sudo update-alternatives --set gnome-www-browser /usr/bin/microsoft-edge-dev
sudo chmod a+x ./00-aliases.sh
sudo cp ./00-aliases.sh /etc/profile.d/
# Windows 10 Theme
mkdir ~/.themes
cd ~/.themes
git clone https://github.com/B00merang-Project/Windows-10.git
git clone https://github.com/B00merang-Project/Windows-10-Dark.git
mkdir ~/.icons
cd ~/.icons
git clone https://github.com/B00merang-Artwork/Windows-10.git
git clone https://github.com/B00merang-Artwork/Windows-10-Dark.git
# Enable services
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service
# Fixes
echo 'Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
        Option "Tapping" "on"
EndSection' > /etc/X11/xorg.conf.d/40-libinput.conf
# Cleanup
